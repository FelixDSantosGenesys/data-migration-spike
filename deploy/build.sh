#!/bin/bash

# fail on any error in any pipe
set -e
set -o pipefail

echo -e "\nCreating build.properties file.\n"
BUILD_PROPERTIES=$(cat <<EOF
AUTO_SUBMIT_CM=${AUTO_SUBMIT_CM}
BRANCH_DEPLOY_PATH=${DEPLOY_PATH}
BRANCH_TO_BUILD=${BRANCH_TO_BUILD}
CLUSTER_NAME=${SERVICE_NAME}
DEPLOY_PATH=${DEPLOY_PATH}
DEPLOY_SCHEDULE=${DEPLOY_SCHEDULE}
DEPLOY_TYPE=${DEPLOY_TYPE}
EMAIL_LIST=${EMAIL_LIST}
GIT_BRANCH=${GIT_BRANCH}
GIT_REPO_URI=${GIT_URL}
SOURCE_BUILD_NAME=${JOB_NAME}
SOURCE_BUILD_NUMBER=${BUILD_NUMBER}
TEST_JOB=${TEST_JOB}
VERSION=${BUILD_NUMBER}
EOF
)

echo "${BUILD_PROPERTIES}" | tee build.properties

# package infrastructure definitions as a zip file for Bricks & Mason to process
echo "Zipping up infra directory."
pushd "${WORKSPACE}/deploy/infra"
rm -f ${WORKSPACE}/infra.zip
zip -r ${WORKSPACE}/infra.zip *
popd
