module.exports = class SessionStorage {
  constructor({ tableName, dynamodbClient }) {
    this.tableName = tableName;
    this.dynamodbClient = dynamodbClient;
  }

  insert(item) {
    return this.dynamodbClient.putItem(this.tableName, item);
  }
};