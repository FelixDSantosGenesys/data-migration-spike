module.exports = class VisitStorage {
  constructor({ tableName, dynamodbClient }) {
    this.tableName = tableName;
    this.dynamodbClient = dynamodbClient;
  }

  scan(options, callback) {

    return this.dynamodbClient.scan(this.tableName, options)
      .then((result) => callback(result))
  }
};