const config = require('../config');
const dynamodb = require('@purecloud/node-dynamodb');
const { DynamoDB } = require('aws-sdk');
const VisitDynamodbStorage = require('./storage/visit-dynamodb-storage');
const SessionDynamodbStorage = require('./storage/session-dynamodb-storage');

const dynamodbClient = dynamodb.getDynamoDriver({ dynamoDb: new DynamoDB(config.dynamodb) });
const visitStorage = new VisitDynamodbStorage({
  dynamodbClient,
  tableName: 'journey-context-service-visits',
});

const sessionStorage = new SessionDynamodbStorage({
  dynamodbClient,
  tableName: 'journey-session-store-sessions',
});

module.exports = {
  visitStorage,
  sessionStorage,
};
