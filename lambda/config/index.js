// const pkg = require('../package');
const {
  buildAppConfig,
  buildDynamodbConfig,
} = require('@altocloud/node-common-config');

module.exports = buildAppConfig('data-migration-spike')
  .extend('dynamodb', buildDynamodbConfig())
  .build();
