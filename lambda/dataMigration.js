const { visitStorage, sessionStorage } = require('./models');
const { ulid } = require('ulid');

// let scannedItems = [];
let numEvaluated = 0;

exports.migrate = async function migrate(data = {}) {
  let {
    Items,
    Count,
    LastEvaluatedKey,
  } = data;

  if(Items) {
    const sessions = Items.map(convertToMinimalSession)
    console.log(sessions)
    sessions.map(async (session) => {
      await sessionStorage.insert(session)
    })
  }

  numEvaluated += Count || 0;

  if (numEvaluated < 200) {
    console.log("Scanning for more...");
    numEvaluated += Count;

    await visitStorage.scan({
      limit:50,
      exclusiveStartKey:LastEvaluatedKey
    }, migrate)
  }
}

function convertToMinimalSession(visit) {
  return Object.assign({}, {
    key: visit.organizationId + '|' + visit.id,
    customerIdIndexKey: visit.organizationId + '|' + visit.customerCookieId + '|cookie',
    sortId: ulid(visit.createdDate),
    organizationId: visit.organizationId,
    exp: visit.exp,
    id: visit.id,
    type: 'web',
    createdDate: visit.createdDate,
    customerId: visit.customerCookieId,
    customerIdType: 'cookie',
    lastEventDate: visit.lastActiveDate,
  })
}